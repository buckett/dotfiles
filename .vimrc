" Use pathogen https://github.com/tpope/vim-pathogen
execute pathogen#infect()
" With MacBooks losing the Fn keys might as well start making use of them.
set pastetoggle=<F10>

