# Use a sensible editor
if which vim 2>&1 > /dev/null; then
        export EDITOR=vim
else
        export EDITOR=vi
fi

# Ignore svn folder and mac finder folder
export FIGNORE=svn:DS_Store:.svn

# Tomcat aliases
alias tchome='export CATALINA_HOME=`pwd`; echo CATALINA_HOME=$CATALINA_HOME'
alias tctail='tail -f ${CATALINA_HOME}/logs/catalina.out'
alias tcless='less ${CATALINA_HOME}/logs/catalina.out'
alias tcclean='rm ${CATALINA_HOME}/logs/catalina.out'
alias tcshow='echo ${CATALINA_HOME}'
alias tc='${CATALINA_HOME}/bin/catalina.sh'

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d /usr/local/go/bin/ ]; then 
  export PATH=/usr/local/go/bin:$PATH
fi

if [ -d $HOME/work ]; then
  export GOPATH=$HOME/work
  export PATH=$GOPATH/bin:$PATH
fi

# This is to allow management of dotfiles, see
# https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  SESSION_TYPE=remote/ssh
else
  SESSION_TYPE=local
fi

# Setup ssh-agent to work across multiple consoles
# https://help.github.com/articles/working-with-ssh-key-passphrases/#platform-windows
if [ $SESSION_TYPE = "local" ]; then
  env=~/.ssh/agent.env

  agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }

  agent_start () {
      (umask 077; ssh-agent >| "$env")
      . "$env" >| /dev/null ; }

  agent_load_env

  # agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
  agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

  if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
      agent_start
      # Don't load on startup.
      # ssh-add
  elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
      if [ 'Darwin' == $(uname) ]; then
           # Adds all identities stored in keychain.
           ssh-add -A > /dev/null
      else 
          ssh-add
      fi
  fi

  unset env
fi

if [ -f $HOME/bin/git-completion.bash ]; then
  source $HOME/bin/git-completion.bash
fi

# This is just a hack for now to keep stuff working on my MacBook
if [ -f $HOME/.bash_profile_osx ]; then
  source $HOME/.bash_profile_osx
fi

if [ -f ~/.vault_pass.txt ]; then
  export ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_pass.txt
fi
